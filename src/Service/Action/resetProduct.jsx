import { Header, Product_Url } from "../Constant/Api"
import { ActionTypes } from "./ActioType"

export const ResetProducts = () => {
  return {
    type: ActionTypes.reset_products,   
  };
};