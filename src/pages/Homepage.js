import React, { useState } from 'react'
import Body from '../componets/Body'
import CardProduct from '../componets/cardproduct'
import Navi from '../componets/navigationbar'
import TabBar from '../componets/TabBar'
import Layout from './Layout'
import Footer from '../componets/footer'
import { connect, useDispatch } from 'react-redux';
import Services from '../componets/Services'
import { useEffect } from 'react'
import { Fetchproduct } from '../Service/Action/FetchAllProduct'
import SkeletonProduct from '../componets/SkeletonCard'
import secureLocalStorage from 'react-secure-storage'
import Joinus from '../componets/Joinus'
import { Fetchcategory } from '../Service/Action/GetCategory'
import { ResetProducts } from '../Service/Action/resetProduct'
import { useNavigate } from 'react-router-dom'
export function Homepage(props) {


  const [isActive,setAct] = useState(false);
  const dispatch = useDispatch()
  const [toggle,setToggle] = useState({
    display:"flex",
    transition:"500ms east-in-out"
  });
  const [hide,setHide] = useState({
    display:"none",
    transition:"500ms east-in-out"
  });
  const showdisplay =()=>{
    {isActive ? setTog(hide) :  setTog(toggle) }
    setAct(!isActive);
 
      
  }

  const [price, setprice] = useState({
    "price_min": "0",
    "price_max": "1000",
    "title": "%"
  })
  const OnPriceHandle = (e) => {
    const { name, value } = e.target;

    setprice(prestate => {
      return {
        ...prestate,
        [name]: value
      }
    })
    console.log(price)
  }

  const OnSubmitPrice = (e) => {
    e.preventDefault()
    redirect(`/filter/${price.price_min}/${price.price_max}/${price.title}`)


  }

  const OnSubmitTitle = (e) => {
    e.preventDefault()
    navi(`/filter/${price.price_min}/${price.price_max}/${price.title}`)
  }
  const redirect = useNavigate();
  const navi = useNavigate()

  const [togglehere,setTog] = useState();
  const [isLoading, SetLoading] = useState(false)

  const [isactive, setActive] = useState();

  const [category, setCategory] = useState([

  ])
  const [back,setBack]= useState(false)

  const [activeclass, setClass] = useState()
  const onHandleCate = (id) => {
    // console.log(id)

    setcid(id)
    props.Fetchproduct(cid)
  }
  const [userAuth, setUserAuth] = useState({
    access_token: ""

  })

  const [cid, setcid] = useState(1);
  useEffect(() => {
    setClass({
      border: "1px solid white",

    })
    SetLoading(true)
    userAuth.access_token = secureLocalStorage.getItem('authlogin');
    setTimeout(() => {

      props.Fetchproduct(-1)

      setProduct(props.product)
      SetLoading(false)

    }, 1000)
   

    // console.log("use effect render")
    console.log(userAuth.access_token)

    Fetchcategory().
      then(res => res.json()).
      then(data => {
        console.log(data);
        setCategory(data)
      })

      return (()=>{

      dispatch(ResetProducts());

     
      })

  }, [back])

  const [pro, setProduct] = useState([])

  return (
    <>
      
      {/* {console.log(props.product)} */}
      {/* {console.log(pro)} */}
      {/* {console.log(isLoading)} */}

      <Navi />
    

      {
        !userAuth.access_token && <Body />
      }

      {
        !userAuth.access_token && <Joinus />
      }
      {
        !userAuth.access_token && <Services />
      }




      {
        userAuth.access_token &&

        <div className='container'>

          <div className="row g-3 my-5" >
            <div className="col-12 ">
              <div className="dropdown d-flex align-items-center ">
              <p className="header display-6 fw-bold">Cateogry</p>
              <button class="sym btn bg-white material-symbols-outlined mx-3 d-block d-md-none" onClick={showdisplay}>
                arrow_drop_down_circle
                 </button>
              </div>
          
              <div className='tab container bg-dark d-md-flex justify-content-center flex-wrap' 
              
              style={
                
                togglehere
              }
              >
                {category && category.map(cate => (


                  <button



                    className='btn rounded-lg mx-3 my-3' onClick={() => onHandleCate(cate.id)}

                    style={activeclass}
                  >
                    <div >
                      {cate.name}


                    </div>

                  </button>

                ))}

              </div>
            </div>

          </div>

          <div className='row my-5'>
            {
              // console.log(category)
            }




          </div>
          {/* <TabBar cate={category} /> */}
          <div className='row g-3 my-5'>

            <div className="col-12 my-5" >

              <p className="header display-6 fw-bold">Filter</p>
              <div className="price-range row" >
                <div className="col-lg-6 my-3 col-12">
                  <div className='d-flex align-items-center'>
                    <label class="form-check-label">Price </label>
                    <input class="form-control mx-3" name="price_min"
                      id=""
                      type="text"
                      placeholder='MIN'
                      onChange={OnPriceHandle}
                    />
                    <input class="form-control mx-3
                 " name="price_max"
                      id="" type="text"
                      placeholder='MAX'
                      onChange={OnPriceHandle}

                    />
                    <button type='submit' className='btn btn-success mx-3' onClick={OnSubmitPrice}>comfirm</button>

                  </div>

                </div>
                <div className="col-lg-6 my-3 col-12">
                  <div className='d-flex align-items-center'>
                    <label class="form-check-label ">Product </label>
                    <input class="form-control mx-5"
                      name="title"
                      id=""
                      type="text"
                      placeholder='Product Name'
                      onChange={OnPriceHandle}

                    />

                    <button className='btn btn-success mx-3 ' type='submit'

                      onClick={OnSubmitTitle}

                    >Search</button>

                  </div>

                </div>









              </div>




            </div>
            {
              isLoading && <SkeletonProduct />

            }


            {pro && props.product.map(product => (


              <CardProduct products={product} 
              setBack ={setBack} back = {back}
              key={product.id} 
             />



            ))}


          </div>

          {/* category  */}

        </div>
      }


      {/* <Footer>
      </Footer> */}

    </>
  )
}








const mtp = (store) => {
  return {
    product: store.productreducer.products
  }
}


export default connect(mtp, { Fetchproduct,ResetProducts })(Homepage)
